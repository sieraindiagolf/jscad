"use strict";
(function () {
  angular.module("util")
    .service("InitService", [
      function InitService () {
        var service = {};

        service.appData = {
          nodes : [],
          links : []
        };

        return service;
      }
    ]);
}());
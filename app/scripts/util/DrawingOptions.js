"use strict";
(function () {
  angular.module("util")
    .value("DrawingOptions", {
      fill: "rgba(255,0,0,1)",
      outline: "rgba(255,0,0,1)"
    });
}());
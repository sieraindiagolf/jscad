"use strict";
(function () {
  angular.module("util")
    .constant("SHAPE_TYPES", {
      SQUARE : "square",
      CIRCLE: "circle"
    });
}());
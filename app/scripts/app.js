'use strict';

angular
  .module('jscadApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',

    "ui.bootstrap",
    "colorpicker.module",

    'util',
    'svg',

    'ui-templates'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

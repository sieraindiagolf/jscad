angular.module('jscadApp')
.controller("MenuController", [
    "$scope", "$rootScope", "SHAPE_TYPES", "DrawingStrategyService", "DrawingOptions", "$modal",
    function MenuController (scope, rootScope, shapeTypes, drawingStrategyService, drawingOptions, modal) {

      scope.shapeTypes = shapeTypes;
      scope.shapes = [
        {
          name: "Basic shapes",
          icon: "brankik brankik-square",
          shapes: "basic",
          shapeButtons : [
            {
              iconCls : 'brankik brankik-square2',
              shapeId: shapeTypes.SQUARE
            },
            {
              iconCls: "brankik brankik-circle",
              shapeId: shapeTypes.CIRCLE
            }
          ]
        }
      ];

      scope.renderingStrategies = drawingStrategyService.getStrategies();
      rootScope.currentRenderingStrategy = "default";

      scope.currentShapeGroup = scope.shapes[0];

      scope.showGrid = true;
      scope.panEnabled = false;

      scope.currentShapeId = null;

      scope.drawingOptions = drawingOptions;

      rootScope.linkActive = false;

      function mc_disablePan () {
        scope.panEnabled = false;
        rootScope.$broadcast("togglePan", scope.panEnabled);
      }

      function mc_disableLink () {
        rootScope.linkActive = false;
      }

      function mc_toggleGrid () {
        scope.showGrid = !scope.showGrid;
        rootScope.$broadcast("toggleGrid", scope.showGrid);
        return false;
      }
      scope.toggleGrid = mc_toggleGrid;

      function mc_togglePan () {
        scope.panEnabled = !scope.panEnabled;
        rootScope.$broadcast("togglePan", scope.panEnabled);
      }
      scope.togglePan = mc_togglePan;

      function mc_getShapeButtons () {
        return "views/menu/shapes/" + scope.currentShapeGroup.shapes.toLowerCase() + ".html"
      }
      scope.getShapeButtons = mc_getShapeButtons;

      function mc_changeShapeGroup (aShapeGroup) {
        scope.currentShapeGroup = aShapeGroup;
      }
      scope.changeShapeGroup = mc_changeShapeGroup;

      function mc_setActiveShape (aShapeId) {
        if (aShapeId === scope.currentShapeId) {
          aShapeId = null;
        }

        if (aShapeId !== null) {
          mc_disablePan();
          mc_disableLink();
        }

        scope.currentShapeId = aShapeId;

        rootScope.$broadcast("mc-select-shape", aShapeId);
      }
      scope.setActiveShape = mc_setActiveShape;

      function mc_changeRenderingStrategy (aStrategyKey) {
        rootScope.currentRenderingStrategy = aStrategyKey;
      }
      scope.changeRenderingStrategy = mc_changeRenderingStrategy;

      function mc_activateLink () {
        rootScope.linkActive = !rootScope.linkActive;
      }
      scope.activateLink = mc_activateLink;

      function mc_save () {
        if (!localStorage) {
          alert("Your browser doesn't support local storage!");
          return;
        }

        modal.open({
          controller: "SaveController",
          scope: scope,
          templateUrl: "views/menu/save-dialog.html"
        });
      }
      scope.save = mc_save;

      function mc_load () {
        if (!localStorage) {
          alert("Your browser doesn't support local storage!");
          return;
        }

        modal.open({
          controller: "LoadController",
          scope: scope,
          templateUrl: "views/menu/load-dialog.html"
        });
      }
      scope.load = mc_load;

      function mc_onLinkChange (aNewValue, aOldValue) {
        if (aNewValue === aOldValue) {
          return;
        }

        if (aNewValue === true) {
          mc_setActiveShape(null);
        }
      }

      function mc_onPanChange (aNewValue, aOldValue) {
        if (aNewValue === aOldValue) {
          return;
        }

        if (aNewValue === true) {
          mc_setActiveShape(null);
        }
      }

      scope.$watch("linkActive", mc_onLinkChange);
      scope.$watch("panEnabled", mc_onPanChange);
    }
]);
"use strict";
(function () {
  angular.module("jscadApp")
    .controller("SaveController", [
      "$scope", "InitService", "$modalInstance",
      function SaveController (scope, initService, modalInstance) {
        scope.name = "";

        function sc_onSave () {
          delete initService.appData.selected;
          localStorage.setItem(scope.name, JSON.stringify(initService.appData));
          modalInstance.dismiss();
        }
        scope.onSave = sc_onSave;
      }
    ]);
}());
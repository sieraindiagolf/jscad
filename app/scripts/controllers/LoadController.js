"use strict";
(function () {
  angular.module("jscadApp")
    .controller("LoadController", [
      "$scope", "$modalInstance", "InitService",
      function LoadController (scope, modalInstance, initService) {
        scope.selectedItem = null;

        scope.items = {};

        (function lc_loadItems () {
          var key;
          for (key in localStorage) {
            scope.items[key] = JSON.parse(localStorage.getItem(key));
          }
        }());

        function lc_onItemClick (aItem) {
          var key;
          for (key in scope.items) {
            scope.items[key].selected = false;
          }

          aItem.selected = true;
          scope.selectedItem = aItem;
        }
        scope.onItemClick = lc_onItemClick;

        function lc_onLoad () {
          if (!scope.selectedItem) {
            return;
          }

          initService.appData = scope.selectedItem;
          scope.$emit("lc-load-drawing");
          modalInstance.dismiss();
        }
        scope.onLoad = lc_onLoad;
      }
    ])
}());
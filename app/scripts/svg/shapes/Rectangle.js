"use strict";
(function () {
  angular.module("svg.shapes")
    .service("Rectangle", [
      function Rectangle () {
        var service = {};

        function r_draw (aNode) {
          var el = document.createElementNS("http://www.w3.org/2000/svg", "rect");

          r_updateShape(el, aNode);

          return el;
        }
        service.createShape = r_draw;

        function r_updateShape (aElement, aNodeData) {

          var width = Math.abs(aNodeData.x - aNodeData.x1);
          var height = Math.abs(aNodeData.y - aNodeData.y1);

          aNodeData.width = width;
          aNodeData.height = height;

          var x = aNodeData.x < aNodeData.x1 ? aNodeData.x : aNodeData.x1;
          var y = aNodeData.y < aNodeData.y1 ? aNodeData.y : aNodeData.y1;

          d3.select(aElement).attr("class", aNodeData.class);
          d3.select(aElement).attr("x", x);
          d3.select(aElement).attr("y", y);
          d3.select(aElement).attr("width", width);
          d3.select(aElement).attr("height", height);

          d3.select(aElement).attr("fill", aNodeData.drawingOptions.fill || "none");
          d3.select(aElement).attr("stroke", aNodeData.drawingOptions.outline || "none");
        }
        service.updateShape = r_updateShape;

        function r_dragShape (aElement, aNode) {
          r_updateShape(aElement, aNode);
        }
        service.dragShape = r_dragShape;

        function r_getLink (aNode) {
          return {
            x : (aNode.x + aNode.x1) / 2,
            y : (aNode.y + aNode.y1) / 2
          };
        }
        service.getLink = r_getLink;

        return service;
      }
    ])
}());
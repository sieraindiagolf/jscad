"use strict";
(function () {
  angular.module("svg.shapes")
    .service("Circle", [
      function Circle () {
        var service = {};

        function c_draw (aNode) {
          var el = document.createElementNS("http://www.w3.org/2000/svg", "circle");

          c_update(el, aNode);

          return el;
        }
        service.createShape = c_draw;

        function c_update (aElement, aNode) {
          var centerX = (aNode.x + aNode.x1) / 2;
          var centerY = (aNode.y + aNode.y1) / 2;
          var radius = Math.sqrt(Math.pow(aNode.x - aNode.x1, 2) + Math.pow(aNode.y - aNode.y1, 2)) / 2;
          aNode.radius = radius;
          aNode.centerX = centerX;
          aNode.centerY = centerY;
          d3.select(aElement).attr("class", aNode.class);
          d3.select(aElement).attr("r",radius);
          d3.select(aElement).attr("cx",centerX);
          d3.select(aElement).attr("cy", centerY);
          d3.select(aElement).attr("fill", aNode.drawingOptions.fill || "none");
          d3.select(aElement).attr("stroke", aNode.drawingOptions.outline || "none");
        }
        service.updateShape = c_update;

        function r_dragShape (aElement, aNode) {
          c_update(aElement, aNode);
        }
        service.dragShape = r_dragShape;

        function r_getLink (aNode) {
          return {
            x: aNode.centerX,
            y: aNode.centerY
          };
        }
        service.getLink = r_getLink;

        return service;
      }
    ])
}());
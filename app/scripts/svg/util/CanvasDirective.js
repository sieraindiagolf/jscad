"use strict"
angular.module("svg.util")
.directive("jsCadCanvas", [
    "$timeout", "InitService",
    function jsCadCanvas (timeout, initService) {
      var directive = {
        templateUrl: "views/svg/util/canvas.html",
        replace: true,
        restrict: "E"
      };

      function jcc_link (aScope, aElement, aAttributes) {
        aScope.width = 0;
        aScope.height = 0;

        function jcc_axisFactory (aScale, aOrient, aTicks) {
          return d3.svg.axis()
            .scale(aScale)
            .orient(aOrient)
            .ticks(aTicks);
        }

        function jcc_zoom () {
          if (d3.event.sourceEvent.type === "mousemove" && aScope.panEnabled !== true) {
            return;
          }

          d3.select("#drawing-content").attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
          d3.select(".grid.x").call(aScope.xAxis);
          d3.select(".grid.y").call(aScope.yAxis);
        }

        function jcc_generateGrid (aHeight, aWidth) {
          aScope.x = d3.scale.linear().domain([aWidth, 0]).range([aWidth,0]);
          aScope.y = d3.scale.linear().domain([0, aHeight]).range([aHeight, 0]);
          var xAxises = parseInt(aWidth / 50);
          var yAxises = parseInt(aHeight / 50);
          aScope.xAxis = jcc_axisFactory(aScope.x, "bottom", xAxises);
          aScope.yAxis = jcc_axisFactory(aScope.y, "left", yAxises);

          d3.select("svg#canvas").append("g")
            .attr("id", "axis");

          d3.select("#axis").append("g")
            .attr("class", "grid x")
            .attr("transform", "translate(36," + (aHeight - 40) + ")")
            .call(aScope.xAxis.tickSize(-aHeight));

          d3.select("#axis").append("g")
            .attr("class", "grid y")
            .attr("transform", "translate(30,-46)")
            .call(aScope.yAxis.tickSize(-aWidth));

          jcc_registerZoom();
        }

        function jcc_registerZoom () {
          var zoom = d3.behavior.zoom()
            .x(aScope.x)
            .y(aScope.y)
            .scaleExtent([1,100])
            .on("zoom", jcc_zoom);
          d3.select("#canvas").call(zoom);
        }

        function jcc_calculateSize () {
          var menuBcr = angular.element("#menu")[0].getBoundingClientRect(),
            bodyBcr = angular.element("body")[0].getBoundingClientRect();

          aElement.attr("height", bodyBcr.height-menuBcr.height - 10);
          aElement.attr("width", "100%");
          aElement.css("margin-top", menuBcr.height);

          jcc_generateGrid(bodyBcr.height - menuBcr.height, bodyBcr.width);
          aScope.$apply();
        }

        function jcc_onToggleGrid () {
          aScope.showGrid = !aScope.showGrid;
        }

        function jcc_onTogglePan (aAngularEvent, aPanValue) {
          aScope.panEnabled = aPanValue;
        }

        aScope.$on("toggleGrid", jcc_onToggleGrid);
        aScope.$on("togglePan", jcc_onTogglePan);
        timeout(jcc_calculateSize, 200, aScope);
      }

      directive.link = jcc_link;

      return directive;
    }
]);
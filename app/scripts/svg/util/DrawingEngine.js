"use strict";
(function () {
  angular.module("svg.util")
    .directive("jsCadDrawingEngine", [
      "$timeout", "DrawingStrategyService", "$rootScope", "InitService","DrawingOptions","DefaultDrawingStrategy", "AbsolutePositionStrategy",
      function DrawingEngine (timeout, drawingStrategyService, rootScope, initService, drawingOptions) {
        var directive = {
          restrict: "A"
        };

        var canvas;
        var referencePoint;
        var currentShape = null;
        var newElement = null;

        function de_link (aScope, aElement, aAttributes) {
          var boundDrawAfterRendering = de_drawAfterRendering.bind(this, true);
          aScope.drawingStrategy = aAttributes.drawingStrategy || "default";
          timeout(boundDrawAfterRendering, 300);
          rootScope.$watch("currentRenderingStrategy", de_drawAfterRendering);
          de_registerMouseListeners();

          canvas = aElement[0];
          referencePoint = canvas.createSVGPoint();

          rootScope.$on("mc-select-shape", de_onSelectShape);
          rootScope.$on("lc-load-drawing", de_drawAfterRendering);
        }

        function de_SVGSpaceTransform (x, y) {
          referencePoint.x = x;
          referencePoint.y = y;
          return referencePoint.matrixTransform(angular.element("#drawing-content")[0].getScreenCTM().inverse());
        }

        function de_onMouseDown () {
          if (currentShape === null) {
            return
          }

          var point = de_SVGSpaceTransform(d3.event.clientX, d3.event.clientY);
          newElement = {
            x: point.x,
            y: point.y,
            x1: point.x,
            y1: point.y,
            shapeId: currentShape,
            class: "node-element",
            drawingOptions: JSON.parse(JSON.stringify(drawingOptions))
          };
          initService.appData.nodes.push(newElement);
          de_drawAfterRendering(rootScope.currentRenderingStrategy, rootScope.currentRenderingStrategy, false);

          d3.select("#canvas").on("mousemove", de_onMouseMove);
        }

        function de_onMouseUp () {
          if (currentShape === null) {
            return;
          }

          var point = de_SVGSpaceTransform(d3.event.clientX, d3.event.clientY);
          newElement.x1 = point.x;
          newElement.y1 = point.y;
          de_drawAfterRendering(rootScope.currentRenderingStrategy, rootScope.currentRenderingStrategy, false);
          d3.select("#canvas").on("mousemove", null);
        }

        function de_onMouseMove () {
          var point = de_SVGSpaceTransform(d3.event.clientX, d3.event.clientY);
          newElement.x1 = point.x;
          newElement.y1 = point.y;
          de_drawAfterRendering(rootScope.currentRenderingStrategy, rootScope.currentRenderingStrategy, false);
        }

        function de_registerMouseListeners () {

          d3.select("#canvas").on("mousedown", de_onMouseDown);
          d3.select("#canvas").on("mouseup", de_onMouseUp);
        }

        function de_drawAfterRendering (aNewValue, aOldValue, aBind) {

          if (aNewValue !== aOldValue) {
            d3.select("#drawing-content").remove();
            d3.select("#canvas").append("g")
              .attr("id", "drawing-content");
            d3.select("#drawing-content").append("g")
              .attr("id", "connectors");
          }

          drawingStrategyService.getStrategy(aOldValue).destroy();
          var drawingStrategy = drawingStrategyService.getStrategy(aNewValue);
          drawingStrategy.draw(aBind);
        }

        function de_onSelectShape (aAngularEvent, aShapeId) {
          currentShape = aShapeId;
        }

        directive.link = de_link;

        return directive;
      }
    ]);
}());
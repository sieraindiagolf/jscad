"use strict";
(function () {
  angular.module("svg.util.strategies")
    .provider("DrawingStrategyService", [
      "DefaultDrawingStrategyProvider",
      function DrawingStrtegyService () {
        var service = {};
        var instance = {};

        var strategies = {};

        function dss_registerStrategy (aName, aStrategy) {
          strategies[aName] = aStrategy;
        }
        instance.registerStrategy = dss_registerStrategy;

        function dss_getStrategy (aName) {
          if (!strategies[aName]) {
            console.warn("The requested strategy is not defined! Rendering with default!");
            return strategies.default;
          }

          return strategies[aName];
        }
        instance.getStrategy = dss_getStrategy;

        function dss_getStrategies () {
          return strategies;
        }
        instance.getStrategies = dss_getStrategies;

        function dss_get () {
          return instance;
        }
        service.$get = dss_get;

        return service;
      }
    ]);
}());
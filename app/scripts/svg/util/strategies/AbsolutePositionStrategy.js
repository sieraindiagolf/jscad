"use strict";
(function () {
  angular.module("svg.util.strategies")
    .service("AbsolutePositionStrategy", [
      "InitService", "DrawingStrategyService", "Circle", "Rectangle", "$rootScope",
      function AbsolutePositionStrategy (initService, drawingStrategyService, circle, rectangle, rootScope) {
        var service = {};

        function aps_SVGSpaceTransform (x, y) {
          var referencePoint = angular.element("#canvas")[0].createSVGPoint();
          referencePoint.x = x;
          referencePoint.y = y;
          return referencePoint.matrixTransform(angular.element("#drawing-content")[0].getScreenCTM().inverse());
        }

        function aps_draw (aBind) {
          var nodes = initService.appData.nodes;
          var links = initService.appData.links;

          var svg = d3.select("#drawing-content");
          var connectors = d3.select("#connectors");

          var link = connectors.selectAll(".link");
          var node = svg.selectAll(".node");

          link = link.data(links);
          link.call(aps_getAndUpdateLinks)
            .enter()
            .append(aps_getAndDrawLink)
            .attr("class", "link")
            .on("contextmenu", aps_removeLink);
          link.exit().remove();

          node = node.data(nodes);
          node.call(aps_getAndUpdateShapes);

          var enter = node.enter();

          enter.append("g").attr("class", "node")
            .append(aps_getAndDrawShape)
            .call(aps_getAndUpdateShapes)
            .on("click", aps_showResizeControls)
            .on("contextmenu", aps_onNodeContextMenu)
            .call(drag);
          node.exit().remove();
        }

        function aps_onNodeContextMenu (aNode) {
          d3.event.stopPropagation();
          d3.event.preventDefault();
          var indexOf = initService.appData.nodes.indexOf(aNode);
          service.selectedNode = null;
          aps_removeLinks(aNode);
          initService.appData.nodes.splice(indexOf, 1);
          d3.selectAll(".node").remove();
          aps_draw();
        }

        function aps_removeLinks (aNode) {
          var linksLength = initService.appData.links.length - 1;
          var currentLink;
          for (linksLength; linksLength >= 0; linksLength--) {
            currentLink = initService.appData.links[linksLength];
            if (currentLink.to === aNode || currentLink.from === aNode) {
              initService.appData.links.splice(linksLength, 1);
            }
          }
        }

        function aps_removeLink (aLink) {
          d3.event.stopPropagation();
          d3.event.preventDefault();
          var indexOf = initService.appData.links.indexOf(aLink);
          initService.appData.links.splice(indexOf, 1);
          aps_draw();
        }

        var lineFunction = d3.svg.line()
          .x(function (node) {return node.x})
          .y(function (node) {return node.y})
          .interpolate("step-before");

        function aps_getAndUpdateLinks () {
          this.each(aps_getAndUpdateLink);
        }

        function aps_getAndUpdateLink (aLink) {
          var fromCoordinates = service[aLink.from.shapeId].getLink(aLink.from);
          var toCoordinates = service[aLink.to.shapeId].getLink(aLink.to);

          d3.select(this).attr("d", lineFunction([
            fromCoordinates,
            toCoordinates
          ]));
        }

        function aps_getAndDrawLink (aLink) {
          var fromCoordinates = service[aLink.from.shapeId].getLink(aLink.from);
          var toCoordinates = service[aLink.to.shapeId].getLink(aLink.to);
          var el = document.createElementNS("http://www.w3.org/2000/svg", "path");

          d3.select(el).attr("d", lineFunction([
            fromCoordinates,
              toCoordinates
          ]));

          return el;
        }

        service.draw = aps_draw;
        service.destroy = function () {};
        service.circle = circle;
        service.square = rectangle;
        service.selectedNode = null;

        function aps_showResizeControls (aNode) {
          var linked = aps_linkElements(aNode);
          aps_removeResizeControls();

          if (aNode.selected || linked === true) {
            return;
          }

          var evt = d3.event;
          var resizeControlsNode = {
            x: aNode.x,
            x1: aNode.x1,
            y: aNode.y,
            y1: aNode.y1,
            drawingOptions: {
              fill: null,
              outline: "#0000ff"
            },
            class: "js-resize-controls"
          };

          aNode.selected = true;
          service.selectedNode = aNode;
          service.resizeControlsNode = resizeControlsNode;

          var resizeControlsEl = function () { return service[aNode.shapeId].createShape(resizeControlsNode) };
          d3.select(evt.currentTarget.parentElement).append(resizeControlsEl);
          evt.stopPropagation();
          d3.select("body").on("click", aps_removeResizeControls);
          d3.select(".js-resize-controls").on("mousedown", aps_startResize);
          d3.select("body").on("mouseup", aps_endResize);
          aps_bringElementToFront(aNode);
        }

        function aps_updateResizeControls () {
          var resizeControls = angular.element(".js-resize-controls")[0];
          service[service.selectedNode.shapeId].updateShape(resizeControls, service.resizeControlsNode);
        }

        function aps_startResize (aNode) {
          d3.event.stopPropagation();
          var boundDoResize = aps_doResize.bind(this, aNode);
          d3.select("body").on("mousemove", boundDoResize);
        }

        function aps_doResize (aNode) {
          d3.event.stopPropagation();
          var svgCoords = aps_SVGSpaceTransform(d3.event.clientX, d3.event.clientY);
          aNode.x1 = svgCoords.x;
          aNode.y1 = svgCoords.y;
          service.resizeControlsNode.x1 = svgCoords.x;
          service.resizeControlsNode.y1 = svgCoords.y;
          aps_draw();
          aps_updateResizeControls();
        }

        function aps_endResize (aNode) {
          if (service.dragging) {
            return;
          }
          d3.event.stopPropagation();
          d3.select("body").on("mousemove", null);
          d3.select("body").on("mouseup", null);
        }

        function aps_removeResizeControls () {
          d3.select(".js-resize-controls").remove();
          d3.select("body").on("click", null);
          if (service.selectedNode) {
            service.selectedNode.selected = false;
          }
        }

        function aps_getAndUpdateShapes () {
          this.each(aps_getAndUpdateShape);
        }

        function aps_getAndUpdateShape (aNodeData) {
          if (service[aNodeData.shapeId]) {
            return service[aNodeData.shapeId].updateShape(d3.select(this).select(".node-element")[0][0], aNodeData);
          } else {
            console.warn("Could not draw shape! Method " + aNodeData.shapeId + " is not defined!");
            return document.createElementNS("http://www.w3.org/2000/svg", "g");
          }
        }

        function aps_getAndDrawShape (aNode) {
          if (service[aNode.shapeId]) {
            return service[aNode.shapeId].createShape(aNode);
          } else {
            console.warn("Could not draw shape! Method " + aNode.shapeId + " is not defined!");
            return document.createElementNS("http://www.w3.org/2000/svg", "g");
          }
        }

        function aps_bringElementToFront (aNode) {
          var indexOfNode = initService.appData.nodes.indexOf(aNode);

          if (indexOfNode === initService.appData.nodes.length - 1) {
            return;
          }

          var nodeReference = initService.appData.nodes[indexOfNode];
          initService.appData.nodes.splice(indexOfNode, 1);
          initService.appData.nodes.push(nodeReference);

          d3.select("#drawing-content").selectAll("g.node").remove();
          aps_draw();
        }

        var drag = d3.behavior.drag()
          .origin(function(d) { return d; })
          .on("dragstart", aps_dragstarted)
          .on("drag", aps_dragged)
          .on("dragend", aps_dragended);

        function aps_dragstarted (aNode) {
          d3.event.sourceEvent.stopPropagation();
          service.dragging = true;
          aps_removeResizeControls();
        }

        function aps_dragged (aNode) {
          aNode.x1 = aNode.x1 - (aNode.x - d3.event.x);
          aNode.y1 = aNode.y1 - (aNode.y - d3.event.y);

          aNode.x = d3.event.x;
          aNode.y = d3.event.y;

          service[aNode.shapeId].dragShape(this, aNode);
          d3.selectAll(".link")
            .data(initService.appData.links)
            .call(aps_getAndUpdateLinks);
        }

        function aps_dragended () {
          service.dragging = false;
          aps_removeResizeControls();
        }

        function aps_linkElements (aNode) {
          if (!rootScope.linkActive) {
            return false;
          }

          if (!service.selectedNode) {
            return false;
          }

          var link = {
            from: service.selectedNode,
            to: aNode
          };
          initService.appData.links.push(link);

          service.selectedNode.selected = false;
          service.selectedNode = null;
          aps_draw();

          return true;
        }

        drawingStrategyService.registerStrategy("default", service);

        return service;
      }
    ]);
}());
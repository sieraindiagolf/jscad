"use strict";
(function () {
  angular.module("svg.util.strategies")
    .factory("DefaultDrawingStrategy", [
      "InitService", "DrawingStrategyService",
      function DefaultDrawingStrategy (initService, drawingStrategyService) {
        var service = {};
        var force;

        function dds_createVertexMap () {
          var vertexMap = [];
          var linksLength = initService.appData.links.length - 1;
          var links = initService.appData.links;
          var nodes = initService.appData.nodes;

          var indexOfFrom, indexOfTo;
          for (linksLength; linksLength>=0; linksLength--) {
            indexOfFrom = nodes.indexOf(links[linksLength].from);
            indexOfTo = nodes.indexOf(links[linksLength].to);
            vertexMap.push({
              source: indexOfFrom,
              target: indexOfTo,
              value: 0
            });
          }


          return vertexMap;
        }

        function dds_draw () {
          var nodes = JSON.parse(JSON.stringify(initService.appData.nodes));
          var links = JSON.parse(JSON.stringify(dds_createVertexMap()));

          var svg = d3.select("#drawing-content");
          var htmlSvg = angular.element("#canvas")[0].getBoundingClientRect();
          force = d3.layout.force()
            .size([htmlSvg.width, htmlSvg.height])
            .charge(0)
            .linkDistance(100)
            .on("tick", tick);

          force.nodes(nodes).links(links).start();

          var link = svg.selectAll(".link");
          var node = svg.selectAll(".node");

          function tick() {
            link.attr("x1", function(d) { return d.source.x; })
              .attr("y1", function(d) { return d.source.y; })
              .attr("x2", function(d) { return d.target.x; })
              .attr("y2", function(d) { return d.target.y; });

            node.attr("cx", function(d) { return d.x; })
              .attr("cy", function(d) { return d.y; });
          }

          link = link.data(links).enter().append("line").attr("class", "link");

          node = node.data(nodes).enter().append("circle").attr("class", "node")
            .attr("r", 12)
            .attr("cx", function (d) {return d.x;})
            .attr("cy", function (d) {return d.y});
        }

        service.draw = dds_draw;

        function dds_destroy () {
          if (force) {
            force.stop();
            force.nodes([]).links([]);
          }
        }
        service.destroy = dds_destroy;

        drawingStrategyService.registerStrategy("force", service);

        return service;
      }
    ])
}());